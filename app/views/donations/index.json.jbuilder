json.array!(@donations) do |donation|
  json.extract! donation, :id, :Date, :Amount, :Donor_id
  json.url donation_url(donation, format: :json)
end
